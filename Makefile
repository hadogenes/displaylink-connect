DESTDIR:=/
INSTALL:=install
DISPLAYLINK_SERVICE:=displaylink-driver.service
USER_SCRIPT_DIR:=/usr/bin
ROOT_SCRIPT_DIR:=/usr/sbin

TARGETS = systemd_system systemd_user user_script root_script xdg_autostart

all: install

install: $(TARGETS)

clean:
	rm -f $(TARGETS) v$(VERSION).tar.gz


systemd_system: $(DESTDIR)/usr/lib/systemd/system/$(DISPLAYLINK_SERVICE).d/displaylink-connect.conf

$(DESTDIR)/usr/lib/systemd/system/$(DISPLAYLINK_SERVICE).d:
	mkdir -p "$(DESTDIR)/usr/lib/systemd/system/$(DISPLAYLINK_SERVICE).d"

$(DESTDIR)/usr/lib/systemd/system/$(DISPLAYLINK_SERVICE).d/displaylink-connect.conf: $(DESTDIR)/usr/lib/systemd/system/$(DISPLAYLINK_SERVICE).d systemd/system/displaylink.service.d/displaylink-connect.conf.in
	sed "s;@ROOT_SCRIPT_DIR@;$(ROOT_SCRIPT_DIR);" systemd/system/displaylink.service.d/displaylink-connect.conf.in > "$(DESTDIR)/usr/lib/systemd/system/$(DISPLAYLINK_SERVICE).d/displaylink-connect.conf"



systemd_user: $(DESTDIR)/usr/lib/systemd/user/displaylink-connect.service

$(DESTDIR)/usr/lib/systemd/user:
	mkdir -p "$(DESTDIR)/usr/lib/systemd/user"

$(DESTDIR)/usr/lib/systemd/user/displaylink-connect.service: $(DESTDIR)/usr/lib/systemd/user systemd/user/displaylink-connect.service.in
	sed "s;@USER_SCRIPT_DIR@;$(USER_SCRIPT_DIR);" systemd/user/displaylink-connect.service.in > "$(DESTDIR)/usr/lib/systemd/user/displaylink-connect.service"

user_script: $(DESTDIR)/$(USER_SCRIPT_DIR)/displaylink_connect

$(DESTDIR)/$(USER_SCRIPT_DIR)/displaylink_connect: displaylink_connect
	$(INSTALL) -Dm755 displaylink_connect "$(DESTDIR)/$(USER_SCRIPT_DIR)/displaylink_connect"

root_script: $(DESTDIR)/$ROOT_SCRIPT_DIR)/run_as_users_with_display

$(DESTDIR)/$ROOT_SCRIPT_DIR)/run_as_users_with_display: run_as_users_with_display
	$(INSTALL) -Dm755 run_as_users_with_display "$(DESTDIR)/$(ROOT_SCRIPT_DIR)/run_as_users_with_display"


xdg_autostart: $(DESTDIR)/etc/xdg/autostart/displaylink-connect.desktop

$(DESTDIR)/etc/xdg/autostart/displaylink-connect.desktop: displaylink-connect.desktop
	$(INSTALL) -Dm644 displaylink-connect.desktop "$(DESTDIR)/etc/xdg/autostart/displaylink-connect.desktop"
